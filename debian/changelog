fscrypt (0.3.5-1) unstable; urgency=medium

  * New upstream version 0.3.5
  * d/patches: new patch: dont-fail-tests-on-mlock.patch.
    Don't fail tests when locked memory is unavailable.
    Thanks to Steve Langasek (Closes: #1081220)
  * d/control: bump Standards-Version to 4.7.0

 -- Paride Legovini <paride@debian.org>  Sat, 26 Oct 2024 19:51:27 +0200

fscrypt (0.3.4-2) unstable; urgency=medium

  * d/libpam-fscrypt.install: install PAM modules into /usr.
    Thanks to Michael Biebl (Closes: 1061581)

 -- Paride Legovini <paride@debian.org>  Mon, 29 Jan 2024 12:10:15 +0100

fscrypt (0.3.4-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable)

  [ Paride Legovini ]
  * New upstream version 0.3.4. (Closes: #1060021)
  * d/control: replace golang-goprotobuf-dev with golang-google-protobuf-dev
  * d/control: bump Standards-Verstion to 4.6.2 (no changes needed)
  * d/copyright: update copyright years for debian/*
  * d/gbp.conf: revert to debian-branch = debian/sid.
    To better comply with the pkg-go team workflow.
  * d/changelog: fix typo in the 0.3.3-1 stanza
  * d/changelog: update my email address in past changelog entries

 -- Paride Legovini <paride@debian.org>  Mon, 08 Jan 2024 22:57:36 +0100

fscrypt (0.3.3-1) unstable; urgency=medium

  * New upstream version 0.3.3.
    Closes: #1006485, tracking:
     - CVE-2022-25326
     - CVE-2022-25327
     - CVE-2022-25328
  * d/gbp.conf: debian-branch = debian/latest (DEP-14)
  * d/copyright: update copyright years for debian/*

 -- Paride Legovini <paride@debian.org>  Mon, 07 Mar 2022 21:32:01 +0000

fscrypt (0.3.1-1) unstable; urgency=medium

  * New upstream version 0.3.1
  * d/copyright: update the maintainer email address (paride@debian.org)
  * d/control: bump Standards-Version to 4.6.0, no changes needed
  * debian/*: wrap-and-sort -bast (cosmetic)

 -- Paride Legovini <paride@debian.org>  Sun, 31 Oct 2021 17:09:10 +0000

fscrypt (0.2.9-1) unstable; urgency=medium

  * New upstream version 0.2.9.
  * Drop patch: 0001-cmd-fscrypt-fix-32-bit-build (upstreamed).

 -- Paride Legovini <paride@debian.org>  Mon, 15 Jun 2020 21:41:10 +0000

fscrypt (0.2.8-1) unstable; urgency=medium

  * New upstream version 0.2.8.
    - closes: #957239 (https://github.com/google/fscrypt/pull/224)
    - closes: #959531 (https://github.com/google/fscrypt/pull/221)
  * New patch: 0001-cmd-fscrypt-fix-32-bit-build.patch (cherry-pick)

 -- Paride Legovini <paride@debian.org>  Tue, 02 Jun 2020 23:00:54 +0000

fscrypt (0.2.7-1) unstable; urgency=medium

  * New upstream version 0.2.7 (closes: #959163).
  * d/rules: run dh_auto_install with --no-source.
  * d/rules: do not try to force -buildmode=pie.
  * d/control: bump the dh compat level to 13.
  * d/control: use the paride@debian.org alias in Uploaders.

 -- Paride Legovini <paride@debian.org>  Mon, 04 May 2020 13:06:29 +0000

fscrypt (0.2.6-1) unstable; urgency=medium

  * New upstream version 0.2.6 (closes: #952667).
  * d/copyright: drop the Files-Excluded section (not needed).
  * d/control: bump Standards-Version to 4.5.0 (no changes needed).
  * d/rules: the 'crypto' testsuite has been renamed to 'keyring'.

 -- Paride Legovini <paride@debian.org>  Wed, 04 Mar 2020 14:23:52 +0000

fscrypt (0.2.5-2) unstable; urgency=medium

  * The upstream libpam config file is now a template; compile it.
    (Closes: #946155)
  * Build-Depend on m4.

 -- Paride Legovini <paride@debian.org>  Wed, 04 Dec 2019 13:57:04 +0000

fscrypt (0.2.5-1) unstable; urgency=medium

  * New upstream version 0.2.5 (Closes: #945398).
  * Build-Depend on debhelper-compat and drop d/compat.
  * Bump Standards-Version to 4.4.1 (no changes needed).
  * d/control: add Rules-Requires-Root: no.
  * d/rules: drop DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed.
    ld --as-needed is enabled by default on Debian development versions.

 -- Paride Legovini <paride@debian.org>  Sat, 30 Nov 2019 15:20:33 +0000

fscrypt (0.2.4-2) unstable; urgency=medium

  * Use team+pkg-go@tracker.debian.org in Maintainer
  * Versioned dependency on golang-any >= 2:1.10~ (see README.md)
  * Disable filesystem tests (require an ad-hoc filesystem and root)
    Fixes FTBFS on ppc64el.
  * Versioned dependency on dh-golang >= 1.35 (fixes FTBFS with gccgo-8)
  * Make the PAM library installation rules work with gccgo-go

 -- Paride Legovini <paride@debian.org>  Sat, 25 Aug 2018 12:48:36 +0000

fscrypt (0.2.4-1) unstable; urgency=medium

  * New upstream version. Fixes: CVE-2018-6558.
  * Bump Standards-Versions to 4.2.0 (no changes needed)
  * gbp.conf: set upstream-branch = upstream/latest (DEP14)
  * Improve the installation rules for the binary and PAM library
  * Run all the tests but the failing ones instead of disabling all of them
  * Verbose build (DH_VERBOSE = 1)

 -- Paride Legovini <paride@debian.org>  Thu, 23 Aug 2018 07:55:41 +0000

fscrypt (0.2.3-2) unstable; urgency=medium

  * Do not set ‘-buildmode=pie’ on mips/mipsel/mips64el
  * Fix the PAM library installation rule for non-amd64 archs

 -- Paride Legovini <paride@debian.org>  Tue, 10 Apr 2018 14:21:55 +0000

fscrypt (0.2.3-1) unstable; urgency=medium

  * Initial release (Closes: #894406)

 -- Paride Legovini <paride@debian.org>  Mon, 09 Apr 2018 17:06:27 +0000
